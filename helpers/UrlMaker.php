<?php

class UrlMaker {

  public $config = [];
  protected $hasConfig = false;

  public function __construct(array $config = []) {
    $this->setConfig($config);
  }

  public function setConfig(array $config = []) {
    if (isset($config['url'])) {
      $this->config = $config['url'];
    } else {
      $this->config = $config;
    }

    if (!empty($config))
      $this->hasConfig = true;
  }

  public function makeUrl($option, $params = null) {
    if (!$this->hasConfig)
      throw new Exception('invalid config passed to ' . get_called_class());

    $baseUrl = $this->config['scheme'] . $this->config['domain'];

    switch ($option) {
      case 'main-entry':
        return $baseUrl . '/' . $this->config['main-entry'];

      case 'category':
        if (!isset($params['url']))
          throw new Exception('missing param url passing to makeUrl');
        return $baseUrl . $params['url'];

      case 'user-entry':
        if (!isset($params['user_id']))
          throw new Exception('missing param user_id passing to makeUrl');
        return $baseUrl . '/' . str_replace('{user_id}', $params['user_id'], $this->config['user-entry']);

      case 'property':
        if (!isset($params['url']))
          throw new Exception('missing param url passing to makeUrl');
        return $baseUrl . $params['url'];

      default:
        throw new Exception('invalid option given to makeUrl()');
    }
  }

}
