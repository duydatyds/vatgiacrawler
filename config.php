<?php

return [
    'curl_options' => [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER => false,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "utf-8",
        CURLOPT_USERAGENT => "robot",
        CURLOPT_AUTOREFERER => true,
        CURLOPT_CONNECTTIMEOUT => 3600,
        CURLOPT_TIMEOUT => 3600,
        CURLOPT_MAXREDIRS => 100,
    ],
    'url' => [
        'scheme' => 'http://',
        'domain' => 'www.vatgia.com',
        'main-entry' => 'raovat/2588/bat-dong-san.html',
        'user-entry' => 'home/detail_user.php?iUse={user_id}',
    ],
    'categories' => array(
        2589 =>
        array(
            'id' => '2589',
            'url' => '/raovat/2589/mua-ban-nha.html',
            'description' => '• Mua, bán nhà',
            'subcategories' =>
            array(
                0 => '10493',
                1 => '14205',
                2 => '10494',
                3 => '10495',
                4 => '10497',
                5 => '14204',
            ),
        ),
        2590 =>
        array(
            'id' => '2590',
            'url' => '/raovat/2590/mua-ban-dat.html',
            'description' => '• Mua, bán đất',
            'subcategories' =>
            array(
                0 => '10499',
                1 => '10498',
            ),
        ),
        10500 =>
        array(
            'id' => '10500',
            'url' => '/raovat/10500/cho-thue.html',
            'description' => '• Cho thuê',
            'subcategories' =>
            array(
                0 => '10501',
                1 => '2593',
                2 => '10502',
                3 => '3611',
                4 => '2594',
                5 => '3518',
            ),
        ),
        4318 =>
        array(
            'id' => '4318',
            'url' => '/raovat/4318/dich-vu-nha-dat.html',
            'description' => '• Dịch vụ nhà đất',
            'subcategories' => '',
        ),
        3588 =>
        array(
            'id' => '3588',
            'url' => '/raovat/3588/can-thue.html',
            'description' => '• Cần thuê',
            'subcategories' =>
            array(
                0 => '3519',
                1 => '2591',
                2 => '3831',
                3 => '3812',
                4 => '3833',
                5 => '2592',
                6 => '9839',
            ),
        ),
        10493 =>
        array(
            'id' => '10493',
            'url' => '/raovat/10493/biet-thu-nha-chia-lo.html',
            'description' => '• Biệt thự- Nhà chia lô',
            'subcategories' => '',
        ),
        14205 =>
        array(
            'id' => '14205',
            'url' => '/raovat/14205/mat-bang-kinh-doanh.html',
            'description' => '• Mặt bằng kinh doanh',
            'subcategories' => '',
        ),
        10494 =>
        array(
            'id' => '10494',
            'url' => '/raovat/10494/nha-chung-cu.html',
            'description' => '• Nhà chung cư',
            'subcategories' => '',
        ),
        10495 =>
        array(
            'id' => '10495',
            'url' => '/raovat/10495/nha-mat-pho.html',
            'description' => '• Nhà mặt phố',
            'subcategories' => '',
        ),
        10497 =>
        array(
            'id' => '10497',
            'url' => '/raovat/10497/nha-trong-ngo.html',
            'description' => '• Nhà trong ngõ',
            'subcategories' => '',
        ),
        14204 =>
        array(
            'id' => '14204',
            'url' => '/raovat/14204/nha-xuong.html',
            'description' => '• Nhà xưởng',
            'subcategories' => '',
        ),
        10499 =>
        array(
            'id' => '10499',
            'url' => '/raovat/10499/dat-canh-tac-lam-trang-trai-nha-xuong.html',
            'description' => '• Đất canh tác, làm trang  trại, nhà xưởng',
            'subcategories' => '',
        ),
        10498 =>
        array(
            'id' => '10498',
            'url' => '/raovat/10498/dat-tho-cu.html',
            'description' => '• Đất thổ cư',
            'subcategories' => '',
        ),
        10501 =>
        array(
            'id' => '10501',
            'url' => '/raovat/10501/cho-thue-mat-bang-kd-cua-hang-kiot.html',
            'description' => '• Cho thuê mặt bằng KD, cửa hàng, kiot',
            'subcategories' => '',
        ),
        2593 =>
        array(
            'id' => '2593',
            'url' => '/raovat/2593/cho-thue-nha.html',
            'description' => '• Cho thuê nhà',
            'subcategories' => '',
        ),
        10502 =>
        array(
            'id' => '10502',
            'url' => '/raovat/10502/cho-thue-phong-tro-o-ghep.html',
            'description' => '• Cho thuê phòng trọ, ở ghép',
            'subcategories' => '',
        ),
        3611 =>
        array(
            'id' => '3611',
            'url' => '/raovat/3611/cho-thue-van-phong.html',
            'description' => '• Cho thuê văn phòng',
            'subcategories' => '',
        ),
        2594 =>
        array(
            'id' => '2594',
            'url' => '/raovat/2594/cho-thue-dat.html',
            'description' => '• Cho thuê đất',
            'subcategories' => '',
        ),
        3518 =>
        array(
            'id' => '3518',
            'url' => '/raovat/3518/cho-thue-kho-xuong.html',
            'description' => '• Cho thuê kho, xưởng',
            'subcategories' => '',
        ),
        3519 =>
        array(
            'id' => '3519',
            'url' => '/raovat/3519/can-thue-kho-nha-xuong.html',
            'description' => '• Cần thuê kho, nhà xưởng',
            'subcategories' => '',
        ),
        2591 =>
        array(
            'id' => '2591',
            'url' => '/raovat/2591/can-thue-nha-o.html',
            'description' => '• Cần thuê nhà ở',
            'subcategories' => '',
        ),
        3831 =>
        array(
            'id' => '3831',
            'url' => '/raovat/3831/can-thue-phong-hop.html',
            'description' => '• Cần thuê phòng họp',
            'subcategories' => '',
        ),
        3812 =>
        array(
            'id' => '3812',
            'url' => '/raovat/3812/can-thue-phong-tro-sv.html',
            'description' => '• Cần thuê phòng trọ SV',
            'subcategories' => '',
        ),
        3833 =>
        array(
            'id' => '3833',
            'url' => '/raovat/3833/can-thue-van-phong.html',
            'description' => '• Cần thuê văn phòng',
            'subcategories' => '',
        ),
        2592 =>
        array(
            'id' => '2592',
            'url' => '/raovat/2592/can-thue-dat-bai-san.html',
            'description' => '• Cần thuê đất, bãi, sân',
            'subcategories' => '',
        ),
        9839 =>
        array(
            'id' => '9839',
            'url' => '/raovat/9839/mat-bang-kd-cua-hang-kiot.html',
            'description' => '• Mặt bằng KD, cửa hàng, kiot',
            'subcategories' => '',
        ),
    ),
    'locations' => array(
        1 =>
        array(
            'id' => '1',
            'location' => 'Toàn quốc',
        ),
        2 =>
        array(
            'id' => '2',
            'location' => 'Hà Nội',
        ),
        3 =>
        array(
            'id' => '3',
            'location' => 'Hồ Chí Minh',
        ),
        4 =>
        array(
            'id' => '4',
            'location' => 'An Giang',
        ),
        5 =>
        array(
            'id' => '5',
            'location' => 'Bà Rịa - Vũng Tàu',
        ),
        6 =>
        array(
            'id' => '6',
            'location' => 'Bắc Ninh',
        ),
        7 =>
        array(
            'id' => '7',
            'location' => 'Bắc Giang',
        ),
        8 =>
        array(
            'id' => '8',
            'location' => 'Bình Dương',
        ),
        9 =>
        array(
            'id' => '9',
            'location' => 'Bình Định',
        ),
        10 =>
        array(
            'id' => '10',
            'location' => 'Bình Phước',
        ),
        11 =>
        array(
            'id' => '11',
            'location' => 'Bình Thuận',
        ),
        12 =>
        array(
            'id' => '12',
            'location' => 'Bạc Liêu',
        ),
        13 =>
        array(
            'id' => '13',
            'location' => 'Bến Tre',
        ),
        14 =>
        array(
            'id' => '14',
            'location' => 'Bắc Cạn',
        ),
        15 =>
        array(
            'id' => '15',
            'location' => 'Cần Thơ',
        ),
        65 =>
        array(
            'id' => '65',
            'location' => 'Đà Nẵng',
        ),
        17 =>
        array(
            'id' => '17',
            'location' => 'Khánh Hòa',
        ),
        57 =>
        array(
            'id' => '57',
            'location' => 'Trà Vinh',
        ),
        19 =>
        array(
            'id' => '19',
            'location' => 'Thừa Thiên Huế',
        ),
        20 =>
        array(
            'id' => '20',
            'location' => 'Lào Cai',
        ),
        21 =>
        array(
            'id' => '21',
            'location' => 'Quảng Ninh',
        ),
        22 =>
        array(
            'id' => '22',
            'location' => 'Đồng Nai',
        ),
        23 =>
        array(
            'id' => '23',
            'location' => 'Nam Định',
        ),
        24 =>
        array(
            'id' => '24',
            'location' => 'Cà Mau',
        ),
        25 =>
        array(
            'id' => '25',
            'location' => 'Cao Bằng',
        ),
        26 =>
        array(
            'id' => '26',
            'location' => 'Gia Lai',
        ),
        27 =>
        array(
            'id' => '27',
            'location' => 'Hà Giang',
        ),
        28 =>
        array(
            'id' => '28',
            'location' => 'Hà Nam',
        ),
        29 =>
        array(
            'id' => '29',
            'location' => 'Hà Nội 2',
        ),
        30 =>
        array(
            'id' => '30',
            'location' => 'Hà Tĩnh',
        ),
        31 =>
        array(
            'id' => '31',
            'location' => 'Hải Dương',
        ),
        32 =>
        array(
            'id' => '32',
            'location' => 'Hải Phòng',
        ),
        33 =>
        array(
            'id' => '33',
            'location' => 'Hoà Bình',
        ),
        34 =>
        array(
            'id' => '34',
            'location' => 'Hưng Yên',
        ),
        35 =>
        array(
            'id' => '35',
            'location' => 'Kiên Giang',
        ),
        36 =>
        array(
            'id' => '36',
            'location' => 'Kon Tum',
        ),
        37 =>
        array(
            'id' => '37',
            'location' => 'Lai Châu',
        ),
        38 =>
        array(
            'id' => '38',
            'location' => 'Lâm Đồng',
        ),
        39 =>
        array(
            'id' => '39',
            'location' => 'Lạng Sơn',
        ),
        40 =>
        array(
            'id' => '40',
            'location' => 'Long An',
        ),
        41 =>
        array(
            'id' => '41',
            'location' => 'Nghệ An',
        ),
        42 =>
        array(
            'id' => '42',
            'location' => 'Ninh Bình',
        ),
        43 =>
        array(
            'id' => '43',
            'location' => 'Ninh Thuận',
        ),
        44 =>
        array(
            'id' => '44',
            'location' => 'Phú Thọ',
        ),
        45 =>
        array(
            'id' => '45',
            'location' => 'Phú Yên',
        ),
        46 =>
        array(
            'id' => '46',
            'location' => 'Quảng Bình',
        ),
        47 =>
        array(
            'id' => '47',
            'location' => 'Quảng Nam',
        ),
        48 =>
        array(
            'id' => '48',
            'location' => 'Quảng Ngãi',
        ),
        49 =>
        array(
            'id' => '49',
            'location' => 'Quảng Trị',
        ),
        50 =>
        array(
            'id' => '50',
            'location' => 'Sóc Trăng',
        ),
        51 =>
        array(
            'id' => '51',
            'location' => 'Sơn La',
        ),
        52 =>
        array(
            'id' => '52',
            'location' => 'Tây Ninh',
        ),
        53 =>
        array(
            'id' => '53',
            'location' => 'Thái Bình',
        ),
        54 =>
        array(
            'id' => '54',
            'location' => 'Thái Nguyên',
        ),
        55 =>
        array(
            'id' => '55',
            'location' => 'Thanh Hoá',
        ),
        56 =>
        array(
            'id' => '56',
            'location' => 'Tiền Giang',
        ),
        58 =>
        array(
            'id' => '58',
            'location' => 'Tuyên Quang',
        ),
        59 =>
        array(
            'id' => '59',
            'location' => 'Vĩnh Long',
        ),
        60 =>
        array(
            'id' => '60',
            'location' => 'Vĩnh Phúc',
        ),
        61 =>
        array(
            'id' => '61',
            'location' => 'Yên Bái',
        ),
        62 =>
        array(
            'id' => '62',
            'location' => 'Đắc Lắc',
        ),
        66 =>
        array(
            'id' => '66',
            'location' => 'Buôn Mê Thuột',
        ),
        64 =>
        array(
            'id' => '64',
            'location' => 'Đồng Tháp',
        ),
        67 =>
        array(
            'id' => '67',
            'location' => 'Đắc Nông',
        ),
        68 =>
        array(
            'id' => '68',
            'location' => 'Hậu Giang',
        ),
        69 =>
        array(
            'id' => '69',
            'location' => 'Đà Lạt',
        ),
        70 =>
        array(
            'id' => '70',
            'location' => 'Điện Biên',
        ),
    ),
    'update' => false,
];
