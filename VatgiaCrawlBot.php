<?php

require_once 'crawlers/CrawlerInterface.php';

class VatgiaCrawlBot {

    protected $config = [];
    protected $categories = [];
    protected $locations = [];
    protected $crawlers = [];
    protected $urlMaker = null;
    protected $curl = null;

    public function __construct(
    BaseCrawler $categoryCrawler, BaseCrawler $locationCrawler, BaseCrawler $propertyCrawler, UrlMaker $urlMaker, $config = []
    ) {

        $this->crawlers['categoryCrawler'] = $categoryCrawler;
        $this->crawlers['locationCrawler'] = $locationCrawler;
        $this->crawlers['propertyCrawler'] = $propertyCrawler;
        $this->urlMaker = $urlMaker;

        $this->config = require(dirname(__FILE__) . '/config.php');
        $this->mergeConfig($config);

        $this->curl = curl_init();
        curl_setopt_array($this->curl, $this->config['curl_options']);

        $this->_init();
    }

    protected function _init() {
        foreach ($this->crawlers as $crawler) {
            $crawler->setResources($this->curl, $this->urlMaker);
        }
        $this->urlMaker->setConfig($this->config['url']);
        if (!empty($this->config['categories']))
            $this->categories = $this->config['categories'];
        if (!empty($this->config['locations']))
            $this->locations = $this->config['locations'];
    }

    public function __destruct() {
        curl_close($this->curl);
    }

    protected function _validate($config) {
        // TODO: implement logic here
        return true;
    }

    public function mergeConfig(array $config) {
        if ($this->_validate($config)) {
            $this->config = array_merge($this->config, $config);
        }
    }

    public function updateLocations() {
        $locations = [];
        $locations = $this->crawlers['locationCrawler']->crawl(
                $this->crawlers['locationCrawler']->getData($this->makeUrl('main-entry'))
        );

        if (empty($locations))
            return;

        return $this->locations = $locations;
    }

    public function updateCategories() {
        $categories = [];
        $categories = $this->crawlers['categoryCrawler']->crawl(
                $this->crawlers['categoryCrawler']->getData($this->makeUrl('main-entry'))
        );

        if (empty($categories))
            return;

        return $this->categories = $categories;
    }

    protected function _setLocation($locationID) {
        if (!array_key_exists($locationID, $this->locations))
            throw new Exception('invalid location');
        curl_setopt($this->curl, CURLOPT_COOKIE, "raovatPosition={$locationID};");
    }

    protected function _setCategory($categoryID) {
        if (!array_key_exists($categoryID, $this->categories))
            throw new Exception('invalid category');
    }

    public function run($categoryID, $locationID) {
        if ($this->config['update']) {
            $this->updateCategories();
            $this->updateLocations();
        }

        if (empty($this->config['categories']))
            $this->updateCategories();
        if (empty($this->config['locations']))
            $this->updateLocations();
        if (isset($locationID))
            $this->_setLocation($locationID);
        if (isset($categoryID))
            $this->_setCategory($categoryID);
        $url = $this->categories[$categoryID]['url'];
        $data = $this->crawlers['propertyCrawler']
                ->getData($this->makeUrl('category', ['url' => $url]));
        $properties = $this->crawlers['propertyCrawler']->crawl($data);
        return [
            'category' => $this->categories[$categoryID]['description'],
            'location' => $this->locations[$locationID]['location'],
            'properties' => $properties,
        ];
    }

    public function makeUrl($option, $params = null) {
        return $this->urlMaker->makeUrl($option, $params);
    }

}
