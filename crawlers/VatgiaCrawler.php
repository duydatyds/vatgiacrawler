<?php

class VatgiaCrawler extends BaseCrawler {

  public $categories = [];

  public function __construct($config = []) {
    parent::__construct($config);
    $this->crawlers['categoryCrawler'] = new CategoryCrawler();
    $this->crawlers['propertyCrawler'] = new PropertyCrawler();
  }

  public function crawl($data) {
    $this->categories = $this->updateCategories();

    if (empty($this->categories))
      return;

    foreach ($this->categories as &$category) {
      if (is_array($category['subcategory'])) {
        foreach ($category['subcategory'] as &$subcategory) {
          $data = $this->getData($this->makeUrl('category', ['url' => $category['url']]));
          $category['properties'][] = $this->crawlers['propertyCrawler']->crawl($data);
        }
        continue;
      }

      $data = $this->getData($this->makeUrl('category', ['url' => $category['url']]));
      $category['properties'][] = $this->crawlers['propertyCrawler']->crawl($data);
      return $this->categories;
    }
  }

  public function updateCategories() {
    $categories = [];

    $data = $this->crawlers['categoryCrawler']->getData($this->makeUrl('main-entry'));
    $categories = $this->crawlers['categoryCrawler']->crawl($data);

    if (!empty($categories)) {
      foreach ($categories as &$category) {
        if ($category['subcategory']) {
          $category['subcategory'] = $this->crawlers['categoryCrawler']->crawl(
                  $this->crawlers['categoryCrawler']->getData($this->makeUrl('category', ['url' => $category['url']]))
          );
        }
      }
    }

    return $categories;
  }

}
