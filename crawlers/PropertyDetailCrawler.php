<?php

class PropertyDetailCrawler extends BaseCrawler {

  public $signatures = [
    'wrapper' => [
      'div' => "#\<div[^\<\>]+?class\=\"raovat_detail_content_left fl\"[^\<\>]*?\>.*?\<div[^\<\>]+?class\=\"box_comment_raovat box_comment_bottom box_hiden\"[^\<\>]+?id\=\"main_comment\"\>#"
    ],
    'inner' => [
      'name' => "#\<h1\>([^\<\>]+)\<\/h1\>#",
      'price' => "#\<div[^\<\>]+class\=\"detail_price\"[^\<\>]*\>\s?\<div\>\s?\<span\>([^\<\>]+)\<\/span\>#",
      'unit' => "#\<div[^\<\>]+class\=\"detail_price\"[^\<\>]*\>\s?\<div\>\s?\<span\>[^\<\>]*?\<\/span\>\s?\<em\>([^\<\>]*)\<\/em\>#",
      'description' => "#\<div[^\<\>]+class\=\"description_content box_hiden\"[^\<\>]+id\=\"main_description\"\>(.+)\<\/div\>[^\<\>]*?\<div[^\<\>]+id=\"main_comment\"[^\<\>]*\>#",
    // 'expired' => "#\<span[^\<\>]+class\=\"icon-(end)-date\"\>#",
    ]
  ];

  public function __construct($config = []) {
    parent::__construct($config);
    $this->crawlers['imageCrawler'] = new ImageCrawler();
  }

  public function crawl($data) {
    $matches = [];
    if (!preg_match($this->signatures['wrapper']['div'], $data, $matches))
      return;

    $output = $this->_formatResult($matches[0]);
    $output['images'] = $this->crawlers['imageCrawler']->crawl($matches[0]);

    return $output;
  }

}
