<?php

class PropertyCrawler extends BaseCrawler {

  protected $signatures = [
    'wrapper' => [
      'div' => "#\<table[^\<\>]+class\=\"raovat_listing_fashion\"[^\<\>]*?\>(.*?)\<\/table\>#",
      'tr' => "#\<tr[^\<\>]+class\=\"data\"[^\<\>]*\>(.*?)\<\/tr\>#",
    ],
    'inner' => [
      'location' => "#\<script[^\<\>]+type\=\"text/javascript\"\>document\.write\(raovatCountry\(\"\d+\"\, \"[^\"]+\"\, \"([^\"]+)\"\)\)\;\<\/script\>#",
      'url' => "#\<a[^\<\>]+class\=\"raovat_name tooltip\"[^\<\>]+href\=\"([^\<\>]+)+\"\>#",
      'time' => "#\<td[^\<\>]+class\=\"update\"\>\s\<span[^\<\>]+title\=\"[^\<\>]+(\d{2}\-\d{2}\-\d{4} \d{2}\:\d{2}\:\d{2})\"\>#",
    ]
  ];

  public function __construct($config = []) {
    parent::__construct($config);
    $this->crawlers['detailCrawler'] = new PropertyDetailCrawler();
    $this->crawlers['userCrawler'] = new UserCrawler();
  }

  public function crawl($data) {
    $matches = [];
    if (!preg_match($this->signatures['wrapper']['div'], $data, $matches))
      return;

    $sub_matches = [];
    if (!preg_match_all($this->signatures['wrapper']['tr'], $matches[0], $sub_matches))
      return;

    $properties = [];
    foreach ($sub_matches[0] as $input) {
      $properties[] = $this->_formatResult($input);
    }

    foreach ($properties as &$property) {
      $url = $this->makeUrl('property', ['url' => $property['url']]);
      $data = $this->getData($url);
      $property['detail'] = $this->crawlers['detailCrawler']->crawl($data);
      $property['user'] = $this->crawlers['userCrawler']->crawl($data);
    }

    return $properties;
  }

}
