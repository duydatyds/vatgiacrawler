<?php

interface CrawlerInterface {

  public function crawl($data);

  public function getData($url);
}

abstract class BaseCrawler implements CrawlerInterface {

  protected $signatures = [
    'wrapper' => [],
    'inner' => [],
  ];
  protected $filters = [
    ''
  ];
  protected $crawlers = [];
  protected $curl = null;
  protected $urlMaker = null;

  public function __construct($config = []) {
    
  }

  public function getData($url) {
    if (!isset($this->curl)) {
      throw new Exception('curl is not set in ' . get_called_class());
    }

    curl_setopt($this->curl, CURLOPT_URL, $url);
    return curl_exec($this->curl);
  }

  public function setResources($curl, UrlMaker $urlMaker) {
    $this->curl = $curl;
    $this->urlMaker = $urlMaker;

    foreach ($this->crawlers as $crawler) {
      $crawler->setResources($curl, $urlMaker);
    }
  }

  public function makeUrl($option, $params = null) {
    if (!isset($this->urlMaker)) {
      throw new Exception('urlMaker is not set in ' . get_called_class());
    }
    return $this->urlMaker->makeUrl($option, $params);
  }

  protected function _formatResult($input) {
    $output = [];
    $matches = [];

    foreach ($this->signatures['inner'] as $key => $signature) {
      $output[$key] = preg_match($signature, $input, $matches) ? trim($matches[1]) : "";
    }

    return $output;
  }

  abstract public function crawl($data);
}

// require all classes implement this interface
// later implement an autoload function to handle loading classes
require_once 'CategoryCrawler.php';
require_once 'LocationCrawler.php';
require_once 'PropertyCrawler.php';
require_once 'UserCrawler.php';
require_once 'PropertyDetailCrawler.php';
require_once 'ImageCrawler.php';
require_once 'VatgiaCrawler.php';
