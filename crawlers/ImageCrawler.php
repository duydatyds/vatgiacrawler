<?php

class ImageCrawler extends BaseCrawler {

  protected $signatures = [
    'wrapper' => [
      'div' => "#\<div[^\<\>]+?class\=\"sliderThumbnail\"\>.*?\<div[^\<\>]+?class\=\"clear\"\>#",
      'a' => "#\<a[^\<\>]+?class\=\"fancybox\"[^\<\>]+?href\=\"[^\<\>\"]+\"[^\<\>]+\>.*?\<\/a\>#"
    ],
    'inner' => [
      'image' => "#\<a[^\<\>]+?class\=\"fancybox\"[^\<\>]+?href\=\"([^\<\>\"]+)\"#",
      'description' => "#\<img[^\<\>]+alt\=\"([^\<\>\"]+)\"#"
    ]
  ];

  public function crawl($data) {
    $matches = [];
    if (!preg_match($this->signatures['wrapper']['div'], $data, $matches))
      return;

    $sub_matches = [];
    if (!preg_match_all($this->signatures['wrapper']['a'], $matches[0], $sub_matches))
      return;

    $images = [];
    foreach ($sub_matches[0] as $input) {
      $images[] = $this->_formatResult($input);
    }

    return $images;
  }

}
