<?php

class CategoryCrawler extends BaseCrawler {

  public $signatures = [
    'wrapper' => [
      'ul' => "#\<ul[^\<\>]+?class\=\"list_category raovat_category_list\"[^\<\>]*?\>.*?\<\/ul\>#",
      'a' => "#\<a[^\<\>]+?\>[^\<\>]*?\<\/a\>#",
    ],
    'inner' => [
      'id' => "#iData\=\"(\d+)\"#",
      'url' => "#href\=\"(.+?)\"#",
      'description' => "#\<a.+?\>(.*?)\<\/a\>#",
      'subcategories' => "#class\=\"(has\_child)\"#",
    ]
  ];

  public function crawl($data) {
    $matches = [];
    if (!preg_match($this->signatures['wrapper']['ul'], $data, $matches))
      return;

    $sub_matches = [];
    if (!preg_match_all($this->signatures['wrapper']['a'], $matches[0], $sub_matches))
      return;

    $categories = [];
    foreach ($sub_matches[0] as $input) {
      $categories[] = $this->_formatResult($input);
    }

    $categories = $this->_rearrange($categories);

    $subcategories = [];
    foreach ($categories as &$category) {
      if ($category['subcategories']) {
        $subcategories = $this->crawl(
                $this->getData($this->makeUrl('category', ['url' => $category['url']]))
        );
        $categories += $subcategories;
        $category['subcategories'] = [];
        foreach ($subcategories as $subcategory) {
          $category['subcategories'][] = $subcategory['id'];
        }
      }
    }

    return $categories;
  }

  private function _rearrange(array $categories) {
    $swap = [];
    foreach ($categories as $category) {
      $swap[$category['id']] = $category;
    }
    return $swap;
  }

}
