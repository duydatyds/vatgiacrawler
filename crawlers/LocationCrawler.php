<?php

class LocationCrawler extends BaseCrawler {

  public $signatures = [
    'wrapper' => [
      'select' => "#\<select[^\<\>]+\/raovat\/change_country\.php[^\<\>]+\>.*?\<\/select\>#",
      'option' => "#\<option[^\<\>]+\>.*?\<\/option\>#",
    ],
    'inner' => [
      'id' => "#\<option[^\<\>]+value\=\"(\d+)\"[^\<\>]*\>.*?\<\/option\>#",
      'location' => "#\<option[^\<\>]+title\=\"([^\<\>\"\"]+)\"[^\<\>]*\>.*?\<\/option\>#",
    ]
  ];

  public function crawl($data) {
    $matches = [];
    if (!preg_match($this->signatures['wrapper']['select'], $data, $matches))
      return;

    $sub_matches = [];
    if (!preg_match_all($this->signatures['wrapper']['option'], $matches[0], $sub_matches))
      return;

    $locations = [];
    foreach ($sub_matches[0] as $input) {
      $locations[] = $this->_formatResult($input);
    }

    $locations = $this->_rearrange($locations);
    return $locations;
  }

  private function _rearrange(array $locations) {
    $swap = [];
    foreach ($locations as $location) {
      $swap[$location['id']] = $location;
    }
    return $swap;
  }

}
