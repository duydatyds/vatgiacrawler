<?php

class UserCrawler extends BaseCrawler {

  public $signatures = [
    'wrapper' => [
      'div' => "#\<div[^\<\>]+?class\=\"raovat_detail_header_right\"[^\<\>]*?\>.*?\<\/div\>#"
    ],
    'inner' => [
      'id' => "#\/raovat\/type\_user\.php\?iUse\=(\d+)#",
      'name' => "#\<li\>[^\<\>]*\<i[^\<\>]+?class\=\"user\_icon\"[^\<\>]*\>[^\<\>]*\<\/i\>[^\<\>]?([^\<\>]+)\<\/li\>#",
      'address' => "#\<li\>[^\<\>]*\<i[^\<\>]+?class\=\"location\_icon\"[^\<\>]*\>[^\<\>]*\<\/i\>[^\<\>]?([^\<\>]+)\<\/li\>#",
      'phone' => "#\<li\>[^\<\>]*\<i[^\<\>]+?class\=\"phone\_icon\"[^\<\>]*\>[^\<\>]*\<\/i\>[^\<\>]?([^\<\>]+)\<\/li\>#",
      'email' => "#\<li\>[^\<\>]*\<i[^\<\>]+?class\=\"mail\_icon\"[^\<\>]*\>[^\<\>]*\<\/i\>[^\<\>]?([^\<\>]+)\<\/li\>#"
    ]
  ];

  public function crawl($data) {
    $matches = [];
    if (!preg_match($this->signatures['wrapper']['div'], $data, $matches))
      return;

    return $this->_formatResult($matches[0]);
  }

}
